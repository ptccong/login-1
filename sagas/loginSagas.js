import { call } from 'redux-saga/effects';
import {  Alert } from 'react-native';


function* postLoginAction(username, password) {
  if (username.length == 0 || password.length == 0) {
    Alert.alert("Warning", "Enter your username and password")
  } else {
    const response = yield fetch('http://5e032d0763d08b0014a28d2b.mockapi.io/test7/user', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',

      },
      body: JSON.stringify({ username: username, password: password, }),

    });
    Alert.alert("Seccess", "Login sccess",[{text:'OK',},{text:'cancel'}  ]);
    //yield console.log(`response = ${JSON.stringify(response)}`);
    //return yield (response.status === 201);//true or false
  }
}
export default function* (action) {
  //console.log('Login Saga - Action', action);
  yield call(postLoginAction, action.payload.username, action.payload.password);
}
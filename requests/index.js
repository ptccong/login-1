import { create } from 'apisauce';
var url='http://5e032d0763d08b0014a28d2b.mockapi.io/test7/user'
	
const request = create({
  baseURL: url,
});

function login(username, password) {

  //console.log(`Request: ${username} ${password}`);
  return request
    .post('/sign_in')
    .then(response => {
      console.log('Request response', response);
      return {
        access_token: response.data.access_token,
        refresh_token: response.data.refresh_token,
      };
    })
    .catch(err => {
      console.log(err);
    });
}

export default {
  login,
};
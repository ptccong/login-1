import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import LoginFrom from '../cong/LoginForm';
import profile from '../cong/profile';

const MainNavigator = createStackNavigator({
  LoginForm: {screen: LoginForm},
  profile: {screen: profile},
});

const App = createAppContainer(MainNavigator);

export default App;
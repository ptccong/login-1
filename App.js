import React from 'react';
import LoginForm from './LoginForm'
import { Provider } from 'react-redux'; 
import { AppRegistry } from 'react-native';
import redux from '../cong/Redux/redux';

 
const App = () => {
  return (
    < Provider store={redux.store}>
      <LoginForm />
    </Provider>
  )
}
export default App;
AppRegistry.registerComponent('cong', () => App);
import React, { Component, useState } from 'react';
import { View, Text, TextInput } from 'react-native';
import {
  Button,
} from 'native-base';
import { connect } from 'react-redux';
import { actionLogin } from '../cong/Actions/loginAction';


@connect(
  state => ({
    login: loginReducer,
  }),
  { actionLogin },
)
function LoginForm() {
  const [username, setUser] = useState()
  const [password, setPass] = useState()
  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     username: '',
  //     password: '',
  //   };
  // }

  handleLoginAction=(event)=> {
    this.props.actionLogin({
      username: username,
      password: password,
    });
  }

  // handleUsername(text, e) {
  //   this.setState({
  //     username: text,
  //   });
  // }

  // handlePassword(text, e) { 
  //   this.setState({
  //     password: text,
  //   });
  // }

  
    //const {navigate} = this.props.navigation;
    return (
      <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
        <Text style={{ fontSize: 35, color: 'white', backgroundColor: '#841584', borderRadius: 5, width: 200, textAlign: 'center', fontWeight: 'bold' }}>Noname</Text>
        <TextInput
          style={{ height: 40, width: 310, borderColor: '#FFFF00', borderWidth: 2, backgroundColor: '#fff', borderRadius: 5, margin: 10, padding: 5, }}
          placeholder="username"
          onChangeText={user => setUser({ user })}
          value={username}>
        </TextInput>
        <TextInput
          style={{ height: 40, width: 310, borderColor: '#00FFFF', borderWidth: 2, backgroundColor: '#fff', borderRadius: 5, margin: 10, padding: 5 }}
          placeholder="password"
          onChangeText={pass => setPass({ pass })}
          value={password}
          secureTextEntry={true}
        >
        </TextInput>
        <Button color='#841584' onPress={this.handleLoginAction.bind(this)} >
          <Text style={{ width: 310, textAlign: 'center', color: '#fff' }}>LOGIN</Text>
        </Button>
      </View>
    );
  
}